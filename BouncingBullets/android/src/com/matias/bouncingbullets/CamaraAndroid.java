package com.matias.bouncingbullets;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.widget.Toast;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Gestiona las funciones especificas de android.
 */
public class CamaraAndroid extends AndroidApplication implements PlatformSpecific {

    /**
     * Aplicación a la que pertenece la camara.
     */
    private Activity context;

    /**
     * Constructor de CamaraAndroid
     * @param context Aplicación a la que pertenece la camara.
     */
    public CamaraAndroid(Activity context) {
        this.context = context;
    }

    /**
     * Constructor vacio de CamaraAndroid
     */
    public CamaraAndroid() {

    }

    /**
     * Permite al usuario tomar una foto.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void hacerFoto() {

        if(context.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED){
            avisarUsuario(Main.idioma.get("errorcamara"));
        }else{
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (i.resolveActivity(context.getPackageManager()) != null) {
                try {
                    AndroidLauncher.fotoArchivo = crearImagen();
                } catch (IOException ex) {
                    avisarUsuario(Main.idioma.get("errorarchivo"));
                }
                if (AndroidLauncher.fotoArchivo != null) {
                    Uri uri = FileProvider.getUriForFile(context,
                            "com.matias.bouncingbullets.fileprovider",
                            AndroidLauncher.fotoArchivo);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    context.startActivityForResult(i, 1);
                }
            }
        }
    }

    /**
     * Crea un archivo temporal para almacenar la imagen.
     * @return Archivo creado.
     * @throws IOException Fallo al crear el archivo.
     */
    private File crearImagen() throws IOException {
        String nombreFoto = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(nombreFoto, ".png", storageDir);
    }

    /**
     * Envia mensajes al usuario en android.
     * @param msg Mensaje a mostrar.
     */
    public void avisarUsuario(final String msg){
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Vibra durante una cantidad dada de milisegundos.
     * @param miliseconds cantidad de milisegundos.
     */
    public void vibrar(int miliseconds){
        if(!Main.desactivarVibracion){
            Gdx.input.vibrate(miliseconds);
        }
    }

    /**
     * Vibra en un patron.
     * @param patern Patrón que se desea seguir al vibrar.
     * @param repeat Posición desde la que se quiere repetir la vibración, -1 si no se quiere repetir.
     */
    public void vibrar(long[] patern,int repeat){
        if(!Main.desactivarVibracion){
            Gdx.input.vibrate(patern,repeat);
        }
    }
}
