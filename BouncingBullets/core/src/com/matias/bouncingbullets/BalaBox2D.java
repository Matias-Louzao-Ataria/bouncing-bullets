package com.matias.bouncingbullets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Actor Bala.
 */
public class BalaBox2D extends BaseActor {

    /**
     * Radio de la bala.
     */
    public static final float RADIO_BALA = 1f;

    /**
     * Última vez que se comprobó su movimiento.
     */
    private int lastChecked;


    /**
     * Constructor de las Balas que genera y establece todo lo necesario.
     * @param world Mundo de la bala.
     * @param texture Textura de la bala.
     * @param posicion Posición de la bala.
     * @param lastChecked Última vez que se comprobó su posición.
     */
    public BalaBox2D(World world, TextureRegion texture, Vector2 posicion, int lastChecked) {
        super(world,texture);
        this.lastChecked = lastChecked;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(posicion);
        bodyDef.bullet = true;
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        this.body = this.world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = CategoryBits.BALA;
        fixtureDef.filter.maskBits = CategoryBits.JUGADOR | CategoryBits.ROCA;
        fixtureDef.filter.groupIndex = 0;
        CircleShape shape = new CircleShape();
        shape.setRadius(RADIO_BALA);
        fixtureDef.shape = shape;
        this.fixture = this.body.createFixture(fixtureDef);
        this.fixture.setUserData(this);
        shape.dispose();
        this.setSize(RADIO_BALA*2,RADIO_BALA*2);
    }

    /**
     * Dibuja el actor en la pantalla
     * @param batch Batch para dibujar el actor
     * @param parentAlpha Transparencia de la pantalla
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        this.setPosition(this.body.getPosition().x-RADIO_BALA,this.body.getPosition().y-RADIO_BALA);
        batch.draw(this.texture,getX(),getY(),getWidth(),getHeight());
    }

    /**
     * Proporciona el valor de la propiedad body.
     * @return Body del actor.
     */
    public Body getBody() {
        return body;
    }

    /**
     * Proporciona la última vez que se comprobó esta bala.
     * @return Última vez que se comprobó la vala.
     */
    public int getLastChecked() {
        return lastChecked;
    }

    /**
     * Esablece la última vez que se comprobó la bala.
     * @param lastChecked Última vez que se comprobó la bala
     */
    public void setLastChecked(int lastChecked) {
        this.lastChecked = lastChecked;
    }

    /**
     * Libera los recursos del actor
     */
    public void dispose(){
        this.body.destroyFixture(this.fixture);
        this.world.destroyBody(this.body);
    }
}
