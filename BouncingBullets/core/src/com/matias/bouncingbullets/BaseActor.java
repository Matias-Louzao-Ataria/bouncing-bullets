package com.matias.bouncingbullets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Base para los actores del juego.
 */
public abstract class BaseActor extends Actor {

    /**
     * Anchura del actor.
     */
    private float WIDTH = 0.5f;

    /**
     * Altura del actor.
     */
    private float HEIGHT = 0.5f;

    /**
     * Mundo al que pertenece el actor.
     */
    protected World world;

    /**
     * Cuerpo del actor.
     */
    protected Body body;

    /**
     * Fixtura del actor.
     */
    protected Fixture fixture;

    /**
     * Textura del actor.
     */
    protected TextureRegion texture;

    /**
     * Constructor de BaseActor
     * @param world Mundo al que pertenece el actor.
     * @param texture Textura del actor.
     */
    public BaseActor(World world, TextureRegion texture){
        this.world = world;
        this.texture = texture;
    }

    /**
     * Constructor vacio de BaseActor
     */
    protected BaseActor() {
    }

    /**
     * Ejecuta la actuación del actor
     * @param delta Tiempo que ha parasado desde el último fotograma.
     */
    @Override
    public void act(float delta) {

    }

    /**
     * Dibuja el actor
     * @param batch Batch para dibujar el actor.
     * @param parentAlpha Transparencia del actor.
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {

    }

    /**
     * Proporciona la anchura del actor.
     * @return Devuelve la anchura del actor.
     */
    public float getWIDTH() {
        return WIDTH;
    }

    /**
     * Modifica la anchura del actor.
     * @param WIDTH Nueva anchura del actor.
     */
    public void setWIDTH(float WIDTH) {
        this.WIDTH = WIDTH;
    }

    /**
     * Proporciona la altura del actor.
     * @return Devuelve la altura del actor.
     */
    public float getHEIGHT() {
        return HEIGHT;
    }

    /**
     * Modifica la altura del actor.
     * @param HEIGHT Nueva altura.
     */
    public void setHEIGHT(float HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    /**
     * Proporciona el cuerpo del actor.
     * @return Cuerpo del jugador.
     */
    public Body getBody() {
        return body;
    }

    /**
     * Libera los recursos del actor.
     */
    public void dispose(){
        this.body.destroyFixture(this.fixture);
        this.world.destroyBody(this.body);
    }
}
