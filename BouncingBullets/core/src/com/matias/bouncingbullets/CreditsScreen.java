package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Pantalla de los creditos.
 */
public class CreditsScreen extends BaseScreen{

    /**
     * Altura de la pantalla.
     */
    private final float WORLD_HEIGHT = Gdx.graphics.getHeight();

    /**
     * Anchura de la pantalla
     */
    private final float WORLD_WIDTH = WORLD_HEIGHT*Main.ASPECT_RATIO;

    /**
     * Scroll de la pantalla.
     */
    private ScrollPane scrollPane;

    /**
     * Aspecto de los elementos de la interfaz de usuario de la pantalla.
     */
    private Skin skin;

    /**
     * Botón de salir.
     */
    private TextButton btnSalir;

    /**
     * Música de la pantalla.
     */
    private Music musica;

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     */
    public CreditsScreen(Game parent, Skin skin) {
        super(parent);
        this.skin = skin;
    }

    /**
     * Prepara los elementos de la interfaz de usuario que se van a mostrar
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT)));
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Main.bitmapFont;
        style.font.getData().setScale(2f,2f);

        this.btnSalir = new TextButton(Main.idioma.get("salida"),this.skin);
        this.btnSalir.setSize(WORLD_WIDTH,(WORLD_HEIGHT/2)*0.25f);
        this.btnSalir.setPosition(0,WORLD_HEIGHT);
        this.btnSalir.addAction(Actions.moveBy(0,-this.btnSalir.getHeight(),1.25f));
        this.btnSalir.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                musica.stop();
                getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new MainMenuScreen(getParent()));
                    }
                })));
            }
        });

        Label lblCreditos = new Label(Main.idioma.get("creditos"),style);
        lblCreditos.setWrap(true);
        getStage().addActor(this.btnSalir);
        this.scrollPane = new ScrollPane(lblCreditos,this.skin);
        this.scrollPane.setSize(WORLD_WIDTH,(WORLD_HEIGHT/2)*1.75f);
        getStage().addActor(this.scrollPane);

        this.musica = ((Main)getParent()).manager.get("bensound/creditos.mp3");
        this.musica.setLooping(true);
        this.musica.setVolume(0.01f);

        this.scrollPane.addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1f)));

        Gdx.input.setInputProcessor(getStage());
        ((Main) getParent()).reproducirMusica(this.musica);
    }

    /**
     * Renderiza la pantalla
     * @param delta Tiempo que ha pasado desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act(delta);
        getStage().draw();
    }

    /**
     * Ajusta la pantalla a un nuevo tamaño
     * @param width Nuevo ancho
     * @param height Nuevo alto
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Libera los recursos del actor.
     */
    @Override
    public void dispose() {
        getStage().dispose();
    }
}
