package com.matias.bouncingbullets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Jugador
 */
public class JugadorBox2D extends BaseActor {

    /**
     * Cantidad de HP que se le permite tener como máximo al jugador.
     */
    public static final int maxHP = 7;

    /**
     * PowerUp del jugador.
     */
    private PowerUpObject.TipoObj powerUp;

    /**
     *  Cantidad de HP del juagdor.
     */
    private int hp = maxHP;

    /**
     * Determina si el jugador puede recivir daño.
     */
    private boolean invencible = false;

    /**
     * Animación del personaje.
     */
    private Array<TextureAtlas.AtlasRegion> animation;

    /**
     * Fotograma actual de la animación.
     */
    private int frame = 0;

    /**
     * Última vez que se dibujó un fotograma.
     */
    private long lastFrame;

    /**
     * Indica si el personaje ha sido dañado.
     */
    private boolean damaged = false;

    /**
     * Momento en el que se le aplica un efecto al jugador.
     */
    private long effectsTimer = 0;

    /**
     * Indica si se ha curado al jugador.
     */
    private boolean cured = false;

    /**
     * Constructor de jugador que genera y establece todo lo necesario.
     * @param world Mundo del jugador.
     * @param texture Textura del jugador.
     * @param posicion Posición del jugador.
     * @param animation Animación del personaje.
     */
    public JugadorBox2D(World world, TextureRegion texture, Vector2 posicion, Array<TextureAtlas.AtlasRegion> animation) {
        super(world,texture);

        setWIDTH(0.7f);
        setHEIGHT(0.7f);

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(posicion);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        this.body = this.world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(getWIDTH(), getHEIGHT());
        fixtureDef.shape = shape;
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = CategoryBits.JUGADOR;
        fixtureDef.filter.maskBits = CategoryBits.BALA | CategoryBits.ROCA | CategoryBits.OBJETO;
        fixtureDef.filter.groupIndex = 0;
        this.fixture = this.body.createFixture(fixtureDef);
        this.fixture.setUserData(this);
        shape.dispose();
        this.setSize(getWIDTH()*2,getHEIGHT()*2);

        this.animation = animation;
        this.lastFrame = TimeUtils.nanoTime();
    }

    /**
     * Dibuja el actor
     * @param batch Batch para dibujar el actor.
     * @param parentAlpha Transparencia del actor.
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(this.invencible){
            batch.setColor(Color.GOLD);
        }

        if(TimeUtils.nanoTime() - this.effectsTimer <= 1000000000){
            if(this.damaged){
                batch.setColor(Color.RED);
            }
            if(this.cured){
                batch.setColor(Color.GREEN);
            }
        }

        this.setPosition(this.body.getPosition().x-getWIDTH(),this.body.getPosition().y-getHEIGHT());

        if(TimeUtils.nanoTime() - this.lastFrame >= 7000000 && this.body.getLinearVelocity().x != 0){
            if(this.frame < this.animation.size-2){
                this.frame++;
            }else{
                this.frame = 0;
            }
            this.lastFrame = TimeUtils.nanoTime();
        }

        if(this.body.getLinearVelocity().x == 0){
            batch.draw(this.animation.get(this.animation.size-1),getX(),getY(),getWidth(),getHeight());
        }else{
            if(this.body.getLinearVelocity().x > 0){
                batch.draw(this.animation.get(frame),getX(),getY(),getWidth(),getHeight());
            }else{
                TextureAtlas.AtlasRegion animacionOriginal = this.animation.get(frame);
                Sprite animacionRotada = new Sprite(animacionOriginal);
                animacionRotada.flip(true,false);
                batch.draw(animacionRotada,getX(),getY(),getWidth(),getHeight());
            }
        }

        if(this.invencible || this.damaged || this.cured){
            batch.setColor(Color.WHITE);
        }
    }

    /**
     * Devuelve la propiedad body.
     * @return Valor de la propiedad body.
     */
    @Override
    public Body getBody() {
        return body;
    }

    /**
     * Proporciona el powerUp del jugador.
     * @return PowerUp del jugador.
     */
    public PowerUpObject.TipoObj getPowerUp() {
        return powerUp;
    }

    /**
     * Modifica el powerUp que tiene el jugador.
     * @param powerUp Nuevo powerUp.
     */
    public void setPowerUp(PowerUpObject.TipoObj powerUp) {
        this.powerUp = powerUp;
    }

    /**
     * Proporciona el HP del jugador.
     * @return HP del jugador.
     */
    public int getHp(){
        return this.hp;
    }

    /**
     * Añade una cantidad de HP indicada al jugador e indica si se le ha dañado o curado.
     * @param hpModificar Cantidad a añadir.
     */
    public void addHp(int hpModificar){
        this.effectsTimer = TimeUtils.nanoTime();
        Main.platformSpecific.vibrar(100);
        if(hpModificar < 0){
            this.damaged = true;
            this.cured = false;
        }else if(hpModificar > 0){
            this.damaged = false;
            this.cured = true;
        }
        this.hp += hpModificar;
    }

    /**
     * Proporciona el valor de la propiedad invencible.
     * @return Valor de la propiedad invencible.
     */
    public boolean isInvencible() {
        return invencible;
    }

    /**
     * Modifica el valor de la propiedad invencible.
     * @param invencible Nuevo valor de la propiedad.
     */
    public void setInvencible(boolean invencible) {
        this.invencible = invencible;
    }

    /**
     * Libera los recursos del actor.
     */
    public void dispose(){
        super.dispose();
    }
}