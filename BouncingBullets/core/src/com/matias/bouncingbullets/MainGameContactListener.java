package com.matias.bouncingbullets;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import static com.matias.bouncingbullets.MainGameScreen.VELOCIDAD_BALA_X;
import static com.matias.bouncingbullets.MainGameScreen.VELOCIDAD_BALA_Y;

/**
 * Gestionador de colisiones de JBox2D.
 */
public class MainGameContactListener implements ContactListener {

    /**
     * Jugador de la pantalla principal de juego.
     */
    private JugadorBox2D jugador;

    /**
     * Mundo al que pertenece el contact listener.
     */
    private World parent;

    public MainGameContactListener(JugadorBox2D jugador,World world){
        this.jugador = jugador;
        this.parent = world;
    }

    /**
     * Detecta la colisión del motos JBox2D.
     * @param contact Información de la colisión.
     */
    @Override
    public void beginContact(Contact contact) {
        Object objA,objB;
        objA = contact.getFixtureA().getUserData();
        objB = contact.getFixtureB().getUserData();

        Body bodyA = contact.getFixtureA().getBody(),bodyB = contact.getFixtureB().getBody();

        if(objA.equals("paredDcha") && objB.getClass() == BalaBox2D.class){
            paredDcha(contact, bodyB);
        }else if(objB.equals("paredDcha") && objA.getClass() == BalaBox2D.class){
            paredDcha(contact, bodyA);
        }

        if(objA.equals("paredIzq") && objB.getClass() == BalaBox2D.class){
            paredIzq(contact, bodyB);
        }else if(objB.equals("paredIzq") && objA.getClass() == BalaBox2D.class){
            paredIzq(contact, bodyA);
        }

        if(objA.equals("paredArr") && objB.getClass() == BalaBox2D.class){
            paredArr(contact, bodyB);
        }else if(objB.equals("paredArr") && objA.getClass() == BalaBox2D.class){
            paredArr(contact, bodyA);
        }

        if(objA.equals("paredAbj") && objB.getClass() == BalaBox2D.class){
            paredAbj(contact, bodyB);
        }else if(objB.equals("paredAbj") && objA.getClass() == BalaBox2D.class){
            paredAbj(contact, bodyA);
        }
        if(objB.getClass() == BalaBox2D.class && objA.getClass() == JugadorBox2D.class){
            balaJugador((JugadorBox2D) objA, (BalaBox2D) objB, bodyA, bodyB);
        }else if(objB.getClass() == JugadorBox2D.class && objA.getClass() == BalaBox2D.class){
            balaJugador((JugadorBox2D) objB, (BalaBox2D) objA, bodyB, bodyA);
        }

        if(objA.getClass() == PowerUpObject.class && objB.getClass() == JugadorBox2D.class){
            aplicarPowerUp((PowerUpObject) objA);
        }else if(objB.getClass() == PowerUpObject.class && objA.getClass() == JugadorBox2D.class){
            aplicarPowerUp((PowerUpObject) objB);
        }
    }

    /**
     * Colisión entre jugador y bala.
     * @param objA Datos del primer cuerpo de la colisión.
     * @param objB Datos del segundo cuerpo de la colisión.
     * @param bodyA Primer cuerpo de la colisión.
     * @param bodyB Segundo cuerpo de la colisión.
     */
    public void balaJugador(JugadorBox2D objA, BalaBox2D objB, Body bodyA, Body bodyB) {
        if (!this.jugador.isInvencible()) {
            objA.addHp(-1);
            MainGameScreen.borrarBalas.add(objB);
        }
        bodyA.setLinearVelocity(0, 0);
        bodyB.setLinearVelocity(bodyB.getLinearVelocity().x * -1, bodyB.getLinearVelocity().y * -1);
    }

    /**
     * Colisión entre un cuerpo y la pared de abajo.
     * @param contact Objeto de la colisión.
     * @param bodyB Cuerpo que colisiona contra la pared.
     */
    public void paredAbj(Contact contact, Body bodyB) {
        bodyB.setLinearVelocity(contact.getFixtureB().getBody().getLinearVelocity().x,0);
        bodyB.applyLinearImpulse(new Vector2(0, VELOCIDAD_BALA_Y),new Vector2(0,0),true);
    }

    /**
     * Colisión entre un cuerpo y la pared de arriba.
     * @param contact Objeto de la colisión.
     * @param bodyB Cuerpo que colisiona contra la pared.
     */
    public void paredArr(Contact contact, Body bodyB) {
        bodyB.setLinearVelocity(contact.getFixtureB().getBody().getLinearVelocity().x,0);
        bodyB.applyLinearImpulse(new Vector2(0,-VELOCIDAD_BALA_Y),new Vector2(0,0),true);
    }

    /**
     * Colisión entre un cuerpo y la pared de la izquierda.
     * @param contact Objeto de la colisión.
     * @param bodyB Cuerpo que colisiona contra la pared.B
     */
    public void paredIzq(Contact contact, Body bodyB) {
        bodyB.setLinearVelocity(0,contact.getFixtureB().getBody().getLinearVelocity().y);
        bodyB.applyLinearImpulse(new Vector2(VELOCIDAD_BALA_X,0),new Vector2(0,0),true);
    }

    /**
     * Colisión entre un cuerpo y la pared de la derecha.
     * @param contact Objeto de la colisión.
     * @param bodyB Cuerpo que colisiona contra la pared.
     */
    public void paredDcha(Contact contact, Body bodyB) {
        bodyB.setLinearVelocity(0,contact.getFixtureB().getBody().getLinearVelocity().y);
        bodyB.applyLinearImpulse(new Vector2(-VELOCIDAD_BALA_X,0),new Vector2(0,0),true);
    }

    /**
     * Aplica el powerUp al jugador y vibra para indicarlo.
     * @param powerUp PowerUp a aplicar.
     */
    private void aplicarPowerUp(PowerUpObject powerUp) {
        Main.platformSpecific.vibrar(new long[] {0,100,100,100},-1);
        powerUp.setDesaparecer(true);
        this.jugador.setPowerUp(powerUp.getTipo());
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
