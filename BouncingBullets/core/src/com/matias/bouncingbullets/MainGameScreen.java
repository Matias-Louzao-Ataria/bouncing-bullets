package com.matias.bouncingbullets;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.*;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Pantallas principal del juego.
 */
public class MainGameScreen extends BaseScreen{
    /**
     * Ancho de las paredes de la pantalla.
     */
    public static final float PARED_WIDTH = 1f;

    /**
     * Alto de la pantalla.
     */
    public static final float WORLD_HEIGHT = 20;

    /**
     * Ancho de la pantalla.
     */
    public static final float WORLD_WIDTH = WORLD_HEIGHT*Main.ASPECT_RATIO;

    /**
     * Velocidad máxima en Y para las balas.
     */
    public static  final  float VELOCIDAD_BALA_X = 80;

    /**
     * Velocidad máxima en Y para las balas.
     */
    public static  final  float VELOCIDAD_BALA_Y = 80;

    /**
     * Posibles velocidades en X para las balas.
     */
    private final Float[] velocidadesX = {0f, VELOCIDAD_BALA_X};

    /**
     * Posibles velocidades en Y para las balas.
     */
    private final Float[] velocidadesY = {0f, VELOCIDAD_BALA_Y};

    /**
     * Mapa que se desea jugar.
     */
    private Mapa nivel;

    /**
     * Mundo de la pantalla.
     */
    private World world;

    /**
     * Personaje del juegador.
     */
    private JugadorBox2D jugador;

    /**
     * Indica si el juego está pausado.
     */
    private boolean pausado = false;

    /**
     * Balas de la pantalla.
     */
    private Array<BalaBox2D> balas;

    /**
     * Balas a borrar en el siguiente fotograma.
     */
    public static Array<BalaBox2D> borrarBalas = new Array<BalaBox2D>();

    /**
     * Rocas de la pantalla
     */
    public Array<RocaBox2D> rocas;

    /**
     * Cuerpo de las paredes.
     */
    private Array<Body> paredesBody;

    /**
     * Fixture de las paredes.
     */
    private Array<Fixture> paredesFixture;

    /**
     * Última vez que se contó un segundo.
     */
    private long  cronometro = 0;

    /**
     * Última vez que se genero una bala.
     */
    private int lastSpawnedBala = 0,

    /**
     *Última vez que se generó un powerUp.
     */
    lastSpawnedPowerUp = 0,

    /**
     * Momento en el que el jugador se hizo invencible.
     */
    cronometroInvencible = 0;

    /**
     * Fuente de la interfaz de usuario.
     */
    private BitmapFont textRenderer;

    /**
     * Stage que se va a mostar durante la pausa.
     */
    private Stage stagePausa;

    /**
     * Stage de final de partida.
     */
    private Stage stageFinal;

    /**
     * Número máximo de balas.
     */
    private int maxBalas,

    /**
     * Minutos transcurridos desde el inicio de la partida.
     */
    min = 0,

    /**
     * Segundos transcurridos desde el inicio de la partida.
     */
    seg = 0,

    /**
     * Espacio entre los corazones de la barra de vida.
     */
    hpBaroffset = 0;

    /**
     * String que representa el cronometro y las puntuaciones leidos del archivo de puntuaciones.
     */
    private String cronometroStr = "", puntuacionesStr = "";

    /**
     * PowerUps en el mapa.
     */
    private Array<PowerUpObject> powerUps;

    /**
     * Anterior posición del dedo del jugador en X.
     */
    private float lastX = 0;

    /**
     * Anterior posición del dedo del jugador en Y.
     */
    private float lastY = 0;

    /**
     * Cantidad a mover el dedo para que el movimiento sea detectado.
     */
    private final float offset = 10;

    /**
     * Imagen que se desea poner de fondo.
     */
    private Texture mapa = null;

    /**
     * Barra de HP del jugador.
     */
    private Array<BaseUIActor> hpBar;

    /**
     * Elemento de interfaz de usuario que representa el powerUp que tiene el juegador
     */
    private BaseUIActor playerPowerUp = null;

    /**
     * Música de la pantalla.
     */
    private Music musica;

    /**
     * Medidas del texto del juego.
     */
    private GlyphLayout glyphLayout;

    /**
     * Botones de la pantalla de fin del juego.
     */
    private Button btnVolverAJugar,btnGuardar,btnSalir;

    /**
     * Botón de la pausa.
     */
    private Button btnPausa;

    /**
     * Botón para quitar la pausa.
     */
    private Button btnPlay;

    /**
     * Tabla que contiene los botones de fin del juego.
     */
    private Table table;

    /**
     * Contiene el cronómetro
     */
    private Label labelCronometro,

    /**
    * Contiene los fotogramas por segundo.
    */
    labelFPS;

    /**
     * Contiene el texto de pausa.
     */
    private Label labelPausa;

    /**
     * Sistema de entrada del juego.
     */
    private InputMultiplexer input;

    /**
     * Tabla de la pantalla de pausa.
     */
    private Table tablaPausa;

    /**
     * Constructor de MainGameScreen.
     * @param parent Juego al que pertenece la pantalla.
     * @param nivel Mapa que se desea jugar.
     */
    public MainGameScreen(Game parent, Mapa nivel) {
        super(parent);
        this.nivel = nivel;
    }

    /**
     * Constructor de MainGameScreen.
     * @param parent Juego al que pertenece la pantalla.
     * @param nivel Mapa que se desea jugar.
     * @param mapa Imagen de fondo que se desea utilizar.
     */
    public MainGameScreen(Game parent,Mapa nivel,Texture mapa){
        this(parent,nivel);
        this.mapa = mapa;
    }

    /**
     * Constructor de MainGameScreen.
     * @param parent Juego al que pertenece la pantalla.
     */
    public MainGameScreen(Game parent) {
        super(parent);
    }

    /**
     * Genera tanto la interfaz de usuario como los elementos del juego antes de comenzar.
     */
    @Override
    public void show() {
        Gdx.input.setCatchKey(Input.Keys.BACK,true);
        this.world = new World(new Vector2(0,0),true);

        setStage(new Stage(new ScalingViewport(Scaling.fit,WORLD_WIDTH,WORLD_HEIGHT)));
        Texture fontTexture = new Texture(Gdx.files.internal("fuenteJuego.png"),true);
        fontTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.MipMapLinearNearest);
        this.textRenderer = new BitmapFont(Gdx.files.internal("fuenteJuego.fnt"),new TextureRegion(fontTexture));

        this.glyphLayout = new GlyphLayout();

        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Main.bitmapFont;
        this.labelCronometro = new Label("",style);
        this.labelCronometro.setFontScale(0.38f,0.5f);
        this.labelCronometro.setPosition(0,(WORLD_HEIGHT/2)-this.labelCronometro.getHeight()/2);

        this.labelFPS = new Label("",style);
        this.labelFPS.setFontScale(0.08f,0.08f);
        this.labelFPS.setPosition(WORLD_WIDTH-5,(WORLD_HEIGHT)-1);

        this.labelPausa = new Label(Main.idioma.get("pausa"),style);
        this.labelPausa.setColor(Color.BLACK);

        getStage().addActor(this.labelCronometro);
        getStage().addActor(this.labelFPS);

        if(this.nivel != null){
            cargarMapa();
        }

        this.powerUps = new Array<PowerUpObject>();

        generarBotones();

        this.stagePausa = new Stage(new ScalingViewport(Scaling.fit,WORLD_WIDTH,WORLD_HEIGHT));

        this.jugador = new JugadorBox2D(this.world,((Main)getParent()).textureAtlas.findRegion("tv"),this.nivel.jugadorSpawn,((Main) getParent()).textureAtlas.findRegions("tv"));
        MainGameContactListener contactListener = new MainGameContactListener(this.jugador, this.world);
        this.lastSpawnedBala = this.getInstanteEnJuego();
        this.balas  = new Array<BalaBox2D>();
        this.paredesBody  = new Array<Body>();
        this.paredesFixture = new Array<Fixture>();
        this.hpBar = new Array<BaseUIActor>();

        this.world.setContactListener(contactListener);
        generarParedes(this.world);

        getStage().addActor(this.jugador);

        configurarInput();

        this.musica =((Main)getParent()).manager.get("bensound/gameplay.mp3") ;
        this.musica.setLooping(true);
        this.musica.setVolume(0.01f);

        getStage().addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1f)));

        this.tablaPausa = new Table();
        this.tablaPausa.setFillParent(true);
        this.labelPausa.setWrap(true);
        this.labelPausa.getStyle().font.getData().setScale(0.2f,0.2f);
        this.tablaPausa.add(this.labelPausa).height(WORLD_HEIGHT/4f).width(WORLD_WIDTH/2);
        this.tablaPausa.row();
        this.tablaPausa.add(this.btnPlay).height(WORLD_HEIGHT/4f).width(WORLD_WIDTH/2f);
        this.tablaPausa.row();
        tablaPausa.add().height(WORLD_HEIGHT/4f).width(WORLD_WIDTH/2);

        this.stagePausa.addActor(this.tablaPausa);

        this.stageFinal = new Stage(new ScalingViewport(Scaling.fit,WORLD_WIDTH,WORLD_HEIGHT));
        this.stageFinal.addActor(this.table);

        this.cronometro = TimeUtils.nanoTime();
        ((Main) getParent()).reproducirMusica(this.musica);
    }

    /**
     *Dibuja en la pantalla la interfaz de usuario, controla el cronómetro, la cantidad de balas y actualiza el estado del juego.
     * @param delta Tiempo transcurrido desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(this.nivel.R,this.nivel.G,this.nivel.B,1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if(!this.pausado){
            this.cronometroStr = String.format("%02d:%02d",this.min,this.seg);
            this.glyphLayout.setText(this.textRenderer,this.cronometroStr);
            if(this.jugador.getHp() > 0){

                if(this.balas.size < this.maxBalas && this.getInstanteEnJuego() - this.lastSpawnedBala >= 1){
                    generarBala();
                }

                gestionPowerUps();

                contadorInvencibilidad();

                if(borrarBalas.size > 0){
                    eliminarBala();
                }

                comprobarBalaSinMoverse();

                incrementarCronometro();

                getStage().getBatch().begin();

                dibujarMapa();

                dibujarInterfazDeUsuario();

                getStage().getBatch().end();
                this.world.step(delta,6,2);
                getStage().act(delta);
                getStage().draw();
            }else{
                Gdx.input.setInputProcessor(this.stageFinal);
                this.stageFinal.act();
                this.stageFinal.draw();
                getStage().getBatch().begin();
                this.textRenderer.draw(getStage().getBatch(),this.cronometroStr,(WORLD_WIDTH/2)-this.glyphLayout.width/2,(WORLD_HEIGHT)-1,WORLD_WIDTH/5f,Align.left,false);
                getStage().getBatch().end();
            }
        }else{
            getStage().draw();
            this.stagePausa.act();
            this.stagePausa.draw();
        }
    }

    /**
     * Carga el mapa que se desea jugar.
     */
    private void cargarMapa() {
        Json json = new Json();

        this.maxBalas = this.nivel.maxBalas;

        this.rocas = new Array<RocaBox2D>();

        for (Vector2 pos : this.nivel.rocas) {
            RocaBox2D roca = new RocaBox2D(this.world, ((Main)getParent()).textureAtlas.findRegion("piedra"),pos);
            this.rocas.add(roca);
            getStage().addActor(roca);
        }
    }

    /**
     * Genrea los controles del juego y los aplica.
     */
    private void configurarInput() {
        this.input = new InputMultiplexer(getStage(), new GestureDetector(new GestureDetector.GestureListener() {
            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                if (pointer == 1) {
                    if (jugador.getPowerUp() != null && jugador.getHp() > 0) {
                        labelCronometro.addAction(Actions.sequence(Actions.fadeOut(0.3f),Actions.fadeIn(0.2f)));
                        switch (jugador.getPowerUp()) {
                            case corazon:
                                if (jugador.getHp() < JugadorBox2D.maxHP) {
                                    jugador.addHp(1);
                                }
                                break;

                            case boton:
                                for (BalaBox2D bala : balas) {
                                    Body body = bala.getBody();
                                    body.setLinearVelocity(body.getLinearVelocity().x * -1, body.getLinearVelocity().y * -1);
                                }
                                break;

                            case chaleco:
                                cronometroInvencible = getInstanteEnJuego();
                                jugador.setInvencible(true);
                                break;
                            default:
                        }
                        jugador.setPowerUp(null);
                    }
                }
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                if (x != lastX && y != lastY && (Math.abs(deltaX) > offset || Math.abs(deltaY) > offset)) {
                    if (Math.abs(deltaX) > offset && Math.abs(deltaY) > offset) {
                        jugador.getBody().setLinearVelocity(new Vector2(Math.signum(deltaX) * 7, Math.signum(-deltaY) * 7));
                    } else if (Math.abs(deltaY) > offset) {
                        jugador.getBody().setLinearVelocity(new Vector2(0, Math.signum(-deltaY) * 7));
                    } else if (Math.abs(deltaX) > offset) {
                        jugador.getBody().setLinearVelocity(new Vector2(Math.signum(deltaX) * 7, 0));
                    }
                    lastX = x;
                    lastY = y;
                }
                return true;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }

            @Override
            public void pinchStop() {

            }
        }), new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                if(keycode == Input.Keys.BACK){
                    return true;
                }
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(float amountX, float amountY) {
                return false;
            }
        });
        Gdx.input.setInputProcessor(input);
    }

    /**
     * Genera los botones de final de partida.
     */
    private void generarBotones() {
        this.table = new Table();
        this.table.setPosition(WORLD_WIDTH/2,(WORLD_HEIGHT/2)-4f);

        this.btnVolverAJugar = crearBoton("botonreplay","botonreplaypulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getParent().setScreen(new MainGameScreen(getParent(),nivel,mapa));
            }
        });

        this.table.add(this.btnVolverAJugar).height(WORLD_HEIGHT/7f).width(WORLD_WIDTH/3f).center();
        this.table.row();

        this.btnGuardar = crearBoton("botonguardar","botonguardarpulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                FileHandle intputRecords = Gdx.files.local("puntuaciones.txt");
                if(intputRecords.exists()){
                    try{
                        InputStream in = intputRecords.read();
                        BufferedInputStream input = new BufferedInputStream(in);
                        int leido = 0;
                        while((leido = input.read()) != -1){
                            puntuacionesStr += (char)leido;
                        }
                    }catch(GdxRuntimeException | IOException e){
                        Main.platformSpecific.avisarUsuario(Main.idioma.get("errorarchivoleer"));
                    }
                }

                intputRecords.delete();

                try{
                    if(nivel != null){
                        cronometroStr = nivel.nombre+";"+cronometroStr;
                    }
                    intputRecords.writeString(puntuacionesStr,true);
                    if(puntuacionesStr.length() > 0){
                        intputRecords.writeString(",",true);
                    }
                    intputRecords.writeString(cronometroStr,true);
                    Main.platformSpecific.avisarUsuario(Main.idioma.get("recordguardado"));
                }catch (GdxRuntimeException e){
                    Main.platformSpecific.avisarUsuario(Main.idioma.get("errorarchivoleer"));
                }

                musica.stop();
                getParent().setScreen(new MainMenuScreen(getParent()));
            }
        });

        this.table.add(this.btnGuardar).height(WORLD_HEIGHT/7f).width(WORLD_WIDTH/3f).center();
        this.table.row();

        this.btnSalir = crearBoton("botonpuerta","botonpuertapulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                musica.stop();
                getParent().setScreen(new MainMenuScreen(getParent()));
            }
        });

        this.table.add(this.btnSalir).height(WORLD_HEIGHT/7f).width(WORLD_WIDTH/3f).center();

        Button.ButtonStyle style = new Button.ButtonStyle();
        style.up = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(("botonpausa"))));
        style.down = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(("botonpausapulsado"))));
        this.btnPausa = new Button(style);
        this.btnPausa.setSize(3,2);
        this.btnPausa.setPosition(WORLD_WIDTH/2-this.btnPausa.getWidth()/2,WORLD_HEIGHT-this.btnPausa.getHeight());
        this.btnPausa.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pausado = true;
                if(!tablaPausa.getChildren().contains(btnSalir,false)){
                    btnSalir.remove();
                    tablaPausa.getCells().get(tablaPausa.getCells().size-1).setActor(btnSalir);
                    for (PowerUpObject p : powerUps) {
                        p.remove();
                    }
                }
                Gdx.input.setInputProcessor(stagePausa);
            }
        });
        getStage().addActor(this.btnPausa);

        Button.ButtonStyle style1 = new Button.ButtonStyle();
        style1.up = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(("botonplay"))));
        style1.down = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(("botonplaypulsado"))));
        this.btnPlay = new Button(style1);
        this.btnPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pausado = false;
                if(!table.getChildren().contains(btnSalir,false)){
                    btnSalir.remove();
                    table.getCells().get(table.getCells().size-1).setActor(btnSalir);
                    for (PowerUpObject p : powerUps) {
                        getStage().addActor(p);
                        p.setSegSpawn(TimeUtils.nanoTime());
                    }
                }
                Gdx.input.setInputProcessor(input);
            }
        });
    }

    /**
     * Comprueba si el jugador es invencible.
     */
    private void contadorInvencibilidad() {
        if(this.jugador.isInvencible() && this.getInstanteEnJuego() - this.cronometroInvencible >= 7){
            this.jugador.setInvencible(false);
        }
    }

    /**
     * Si se le ha pasado un mapa a la pantalla, lo dibuja.
     */
    private void dibujarMapa() {
        if(this.mapa != null){
            getStage().getBatch().draw(this.mapa,0,0,WORLD_WIDTH,WORLD_HEIGHT);
        }
    }

    /**
     * Dibuja en la pantalla la interfaz de usuario.
     */
    private void dibujarInterfazDeUsuario() {
        if(this.playerPowerUp == null && this.jugador.getPowerUp() != null){
            String nombre = "";
            switch (this.jugador.getPowerUp()){
                case corazon:
                    nombre = "pollo";
                    break;

                case chaleco:
                    nombre = "chaleco";
                    break;

                case boton:
                    nombre = "boton2";
                    break;
            }

            this.playerPowerUp = new BaseUIActor(((Main)getParent()).textureAtlas.findRegion(nombre),WORLD_WIDTH- BaseUIActor.WIDTH-8,WORLD_HEIGHT- BaseUIActor.HEIGHT,this.jugador.getPowerUp());
            getStage().addActor(this.playerPowerUp);
        }else if(this.jugador.getPowerUp() == null && this.playerPowerUp != null || this.jugador.getPowerUp() != null && this.playerPowerUp.getTipo() != this.jugador.getPowerUp()){
            this.playerPowerUp.remove();
            this.playerPowerUp = null;
        }

        this.textRenderer.setColor(Color.WHITE);
        this.textRenderer.getData().setScale(0.35f,0.4f);
        this.labelCronometro.setText(cronometroStr);
        if(Main.fps){
            this.labelFPS.setText(String.format("%3d",Gdx.graphics.getFramesPerSecond()));
        }

        if(this.hpBar.size > this.jugador.getHp()){
            updateHp();
        }else if(this.hpBar.size < this.jugador.getHp()){
            addHp();
        }
    }

    /**
     * Incrementa el cronometro.
     */
    private void incrementarCronometro() {
        if(TimeUtils.nanoTime() - this.cronometro >= 1000000000){
            this.cronometro = TimeUtils.nanoTime();
            this.seg++;
            if(this.seg >= 60){
                this.seg = 0;
                this.min++;
            }
        }
    }

    /**
     * Comprueba si alguna bala está sin moverse más de 5 segundo y la mueve si es necesario.
     */
    private void comprobarBalaSinMoverse() {
        for (BalaBox2D bala : this.balas) {
            if(this.getInstanteEnJuego() - bala.getLastChecked() >= 3 && bala.getBody().getLinearVelocity().x == 0 && bala.getBody().getLinearVelocity().y == 0){
                bala.getBody().setLinearVelocity(new Vector2(new int[] {1,-1}[MathUtils.random(0,1)] * velocidadesX[MathUtils.random(1)], new int[] {1,-1}[MathUtils.random(0,1)] * velocidadesY[MathUtils.random(1)]));
                bala.setLastChecked(getInstanteEnJuego());
            }else if(this.getInstanteEnJuego() - bala.getLastChecked() >= 3 && bala.getBody().getLinearVelocity().x != 0 && bala.getBody().getLinearVelocity().y != 0){
                bala.setLastChecked(getInstanteEnJuego());
            }
        }
    }

    /**
     * Genera powerUps y controla cuando tienen que desaparecer.
     */
    private void gestionPowerUps() {
        for (PowerUpObject powerUp:this.powerUps) {
            if(powerUp.isDesaparecer()){
                for (Actor a:getStage().getActors()) {
                    if(a == powerUp){
                        a.remove();
                        powerUp.dispose();
                        this.powerUps.removeValue(powerUp,false);
                    }
                }
            }
        }
        if(this.getInstanteEnJuego() - this.lastSpawnedPowerUp >= 5){
            if(MathUtils.random(1,100) > 80){
                generarPowerUp();
            }
        }
    }

    /**
     *Añade un corazón a la barra de HP del juegador.
     */
    private void addHp() {
        for (int i = 0; i < this.jugador.getHp()-this.hpBar.size; i++) {
            BaseUIActor hp = new BaseUIActor(((Main)getParent()).textureAtlas.findRegion("corazon"),this.hpBaroffset,WORLD_HEIGHT - BaseUIActor.HEIGHT);
            this.hpBar.add(hp);
            getStage().addActor(hp);
            this.hpBaroffset+=2;
        }
    }

    /**
     * Actualiza la cantidad de corazones en la barra de HP si la vida del jugador ha bajado.
     */
    private void updateHp(){
        for (int i = this.hpBar.size;i > this.jugador.getHp(); i--) {
            BaseUIActor hpActual = this.hpBar.get(i-1);
            hpActual.remove();
            this.hpBar.removeValue(hpActual,true);
            this.hpBaroffset-=2;
        }
    }

    /**
     * Genera un power up en la pantalla.
     */
    private void generarPowerUp() {
        int max = PowerUpObject.TipoObj.values().length;
        lastSpawnedPowerUp = getInstanteEnJuego();
        PowerUpObject.TipoObj tipo = PowerUpObject.TipoObj.values()[MathUtils.random(0,max-1)];
        TextureRegion texture = null;
        switch (tipo){
            case boton:
                texture  = ((Main)getParent()).textureAtlas.findRegion("boton2");
                break;
            case chaleco:
                 texture = ((Main)getParent()).textureAtlas.findRegion("chaleco");
                break;
            case corazon:
                texture = ((Main)getParent()).textureAtlas.findRegion("pollo");
                break;
        }
        PowerUpObject powerUp = new PowerUpObject(this.world,texture, tipo,TimeUtils.nanoTime());
        Vector2 nuevaPos = this.nivel.powerUpSpawnPoints.get(MathUtils.random(0,this.nivel.powerUpSpawnPoints.size - 1));
        if(this.powerUps.size > 0){
            while(nuevaPos != null && this.powerUps.size < this.nivel.powerUpSpawnPoints.size){
                if(powerUpComrpobarPosicion(nuevaPos)){
                    powerUp.setPosicion(nuevaPos);
                    this.powerUps.add(powerUp);
                    getStage().addActor(powerUp);
                    nuevaPos = null;
                }else{
                    nuevaPos = this.nivel.powerUpSpawnPoints.get(MathUtils.random(0,this.nivel.powerUpSpawnPoints.size - 1));
                }
            }
        }else{
            powerUp.setPosicion(nuevaPos);
            this.powerUps.add(powerUp);
            getStage().addActor(powerUp);
        }
    }

    /**
     * Comprueba si la posición está disponible para que un objeto aparezca en ella.
     * @param posicion Posición que se desea comprobar.
     * @return True si está disponible y false si no lo está.
     */
    private boolean powerUpComrpobarPosicion(Vector2 posicion){
        for (PowerUpObject actual : this.powerUps) {
            if(actual.getBody().getPosition().equals(posicion)){
                return false;
            }
        }

        return true;
    }

    /**
     * Proporciona la cantidad de segundo que han pasado desde que se inició el juego.
     * @return Cantidad de segundo que han pasado desde que se inició el juego.
     */
    public int getInstanteEnJuego() {
        return (this.min*60)+this.seg;
    }

    /**
     * Crea un botón con un estilo que utiliza las imagenes dadas y un ChangeListener como acción a realizar.
     * @param up Nombre de la imagen del botón sin pulsar en el atlas de texturas.
     * @param down Nombre de la imagen del botón pulsado en el atlas de texturas
     * @param listener Listener que proporciona al botón una acción a realizar cuando se pulsa.
     * @return Botón creado.
     */
    public Button crearBoton(String up,String down,ChangeListener listener) {
        Button.ButtonStyle style = new Button.ButtonStyle();
        style.up = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(up)));
        style.down = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(down)));
        Button test = new Button(style);
        test.addListener(listener);
        return test;
    }

    /**
     * Genera una bala en la pantalla
     */
    private void generarBala() {
        this.lastSpawnedBala = getInstanteEnJuego();
        BalaBox2D bala = new BalaBox2D(this.world, ((Main)getParent()).textureAtlas.findRegion("bala"),this.nivel.balasSpawn.get(MathUtils.random(0,this.nivel.balasSpawn.size-1)), this.getInstanteEnJuego());
        bala.getBody().applyLinearImpulse(new Vector2(velocidadesX[MathUtils.random(1)], velocidadesY[MathUtils.random(1)]),new Vector2(0,0),true);
        this.balas.add(bala);
        getStage().addActor(bala);
    }


    /**
     * Elimina las balas indicadas en el array "borrarBalas".
     */
    private void eliminarBala() {
        int pos = 0;
        for (BalaBox2D bala : borrarBalas) {
            pos = getStage().getActors().indexOf(bala,true);
            if( pos != -1){
                getStage().getActors().removeIndex(pos);
                this.balas.removeIndex(this.balas.indexOf(bala,true));
                borrarBalas.removeIndex(borrarBalas.indexOf(bala,true));
                bala.dispose();
            }
        }
    }

    /**
     * Genera las paredes que actuarán como límite de movimiento en el mundo designado.
     * @param world Mundo en el que se quieren generar las paredes.
     */
    public void generarParedes(World world){
        //Derecha de la pantalla
        PolygonShape paredShapeDcha = new PolygonShape();
        paredShapeDcha.setAsBox(PARED_WIDTH,WORLD_HEIGHT);
        BodyDef paredBodyDefDcha = new BodyDef();
        paredBodyDefDcha.position.set(new Vector2(WORLD_WIDTH+ PARED_WIDTH,WORLD_HEIGHT));
        paredBodyDefDcha.type = BodyDef.BodyType.StaticBody;
        Body paredBodyDcha = world.createBody(paredBodyDefDcha);
        Fixture paredFixtureDcha = paredBodyDcha.createFixture(paredShapeDcha,1);
        paredFixtureDcha.getFilterData().maskBits = CategoryBits.PARED;
        paredFixtureDcha.getFilterData().maskBits = CategoryBits.JUGADOR | CategoryBits.BALA;
        paredFixtureDcha.setUserData("paredDcha");
        this.paredesBody.add(paredBodyDcha);
        this.paredesFixture.add(paredFixtureDcha);

        //Izquierda de la pantalla
        BodyDef paredBodyDefIzq = new BodyDef();
        paredBodyDefIzq.position.set(new Vector2(0 - PARED_WIDTH,WORLD_HEIGHT));
        paredBodyDefIzq.type = BodyDef.BodyType.StaticBody;
        Body paredBodyIzq = world.createBody(paredBodyDefIzq);
        Fixture paredFixtureIzq = paredBodyIzq.createFixture(paredShapeDcha,1);
        paredFixtureIzq.setUserData("paredIzq");
        paredFixtureIzq.getFilterData().maskBits = CategoryBits.PARED;
        paredFixtureIzq.getFilterData().maskBits = CategoryBits.JUGADOR | CategoryBits.BALA;
        this.paredesBody.add(paredBodyIzq);
        this.paredesFixture.add(paredFixtureIzq);

        //Arriba de la pantalla
        PolygonShape paredShapeArr = new PolygonShape();
        paredShapeArr.setAsBox(WORLD_WIDTH, PARED_WIDTH);
        BodyDef paredBodyDefArr = new BodyDef();
        paredBodyDefArr.position.set(new Vector2(0,WORLD_HEIGHT+ PARED_WIDTH -2));
        paredBodyDefArr.type = BodyDef.BodyType.StaticBody;
        Body paredBodyArr = world.createBody(paredBodyDefArr);
        Fixture paredFixtureArr = paredBodyArr.createFixture(paredShapeArr,1);
        paredFixtureArr.setUserData("paredArr");
        paredFixtureArr.getFilterData().maskBits = CategoryBits.PARED;
        paredFixtureArr.getFilterData().maskBits = CategoryBits.JUGADOR | CategoryBits.BALA;
        this.paredesBody.add(paredBodyArr);
        this.paredesFixture.add(paredFixtureArr);

        //Abajo de la pantalla
        BodyDef paredBodyDefAbj = new BodyDef();
        paredBodyDefAbj.position.set(new Vector2(0,0- PARED_WIDTH));
        paredBodyDefAbj.type = BodyDef.BodyType.StaticBody;
        Body paredBodyAbj = world.createBody(paredBodyDefAbj);
        Fixture paredFixtureAbj = paredBodyAbj.createFixture(paredShapeArr,1);
        paredFixtureAbj.getFilterData().maskBits = CategoryBits.PARED;
        paredFixtureAbj.getFilterData().maskBits = CategoryBits.JUGADOR | CategoryBits.BALA;
        paredFixtureAbj.setUserData("paredAbj");
        this.paredesBody.add(paredBodyAbj);
        this.paredesFixture.add(paredFixtureAbj);
        paredShapeArr.dispose();
        paredShapeDcha.dispose();
    }

    /**
     * Destruye las paredes creadas como límites para el juego.
     */
    public void destruirParedes() {
        for (int i = 0; i < this.paredesBody.size; i++) {
            Body bodyActual = this.paredesBody.get(i);
            Fixture fixtureActual = this.paredesFixture.get(i);
            bodyActual.destroyFixture(fixtureActual);
            this.world.destroyBody(bodyActual);
        }
    }


    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        this.jugador.remove();
        this.jugador.dispose();

        for (BalaBox2D balaActual : balas) {
            balaActual.remove();
            balaActual.dispose();
        }

        for (RocaBox2D roca : this.rocas) {
            roca.remove();
            roca.dispose();
        }

        for (PowerUpObject powerUp : this.powerUps) {
            powerUp.remove();
            powerUp.dispose();
        }

        borrarBalas = new Array<BalaBox2D>();

        destruirParedes();

        this.textRenderer.dispose();
        if(this.world != null){
            this.world.dispose();
        }
        getStage().dispose();
        this.stagePausa.dispose();
    }

    /**
     * Gestiona la reapertura de la pantalla.
     */
    @Override
    public void resume() {
        super.resume();
        ((Main) getParent()).reproducirMusica(this.musica);
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
        this.stagePausa.getViewport().update(width,height);
    }

    /**
     * Proporciona el mundo de la pantalla.
     * @return Mundo de la pantalla.
     */
    public World getWorld() {
        return world;
    }

    /**
     * Proporciona el personaje de esta pantalla.
     * @return El personaje de la pantalla.
     */
    public JugadorBox2D getJugador() {
        return jugador;
    }
}
