package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Pantalla de menú principal del juego.
 */
public class MainMenuScreen extends BaseScreen{

    /**
     * Cantidad de botones que hay en la tabla y por lo tanto el porcentage de pantalla que debe de tener en alto.
     */
    public static final float ESCALA_ALTO_BOTON = 4f;

    /**
     * Pad de la parte de abajo de los botones de la tabla.
     */
    public static final float PAD_BOTTOM = 2f;

    /**
     * Altura de la escena.
     */
    public static final int WORLD_HEIGHT = Gdx.graphics.getHeight();

    /**
     * Anchura de la escena
     */
    public static final float WORLD_WIDTH = WORLD_HEIGHT * Main.ASPECT_RATIO;
    public static final float ESCALA_ANCHO_BOTON = 3f;

    /**
     * Aspecto a use en la pantalla.
     */
    private Skin skin;

    /**
     * Botones de la pantalla
     */
    private Button btnJugar,btnOpciones, btnCamara,btnSalir,btnPuntuaciones,btnCreditos,btnLeyenda;

    /**
     * Tectura de fondo.
     */
    private TextureAtlas.AtlasRegion mapa = ((Main)getParent()).textureAtlas.findRegion("testbuttonpulsado");

    /**
     * Tabla de la pantalla.
     */
    private Table table;

    /**
     * Dalogos a mostrar en la pantalla.
     */
    private Dialog dialogoSalir,dialogoCambiar;

    /**
     * Música de la pantalla.
     */
    private Music musica;

    /**
     * Constructor del menú principal del juego.
     * @param parent Juego al que pertenece la pantalla.
     */
    public MainMenuScreen(Game parent) {
        super(parent);
    }

    /**
     * Imagen a usar de fondo en el juego.
     */
    public Texture imagenFondo;

    /**
     * Inicializa el stage que se va a utilizar en esta pantalla, a demás de los dialogos de la misma, una tabla y los componentes de interfaz de usuario que se compondrán
     */
    @Override
    public void show() {

        Main.isFirts = !Gdx.files.local("first").exists();

        if(Gdx.files.local("config").exists()){
            leerConfiguracion();
        }

        setStage(new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT)));
        this.table = new Table();
        this.table.setFillParent(true);
        this.table.setBackground(new SpriteDrawable(new Sprite(this.mapa)));
        this.skin = new Skin(Gdx.files.internal("assets/uiskin.json"));
        this.skin.getFont("default-font").getData().setScale(4f,4f);

        this.dialogoSalir = new Dialog("",this.skin);
        this.dialogoSalir.text(new Label(Main.idioma.get("salir"),this.skin));
        TextButton btnsi = new TextButton(Main.idioma.get("si"),skin);
        btnsi.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                Gdx.app.exit();
            }
        });
        dialogoSalir.button(btnsi);
        dialogoSalir.button(new TextButton(Main.idioma.get("no"),skin));

        this.btnJugar = crearBoton("botonjugar","botonjugarpulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(imagenFondo != null){
                    salir(new Runnable() {
                        @Override
                        public void run() {
                            getParent().setScreen(new MapSelectionScreen(getParent(),skin,imagenFondo,musica));
                        }
                    });
                }else{
                    salir(new Runnable() {
                        @Override
                        public void run() {
                            getParent().setScreen(new MapSelectionScreen(getParent(),skin,musica));
                        }
                    });
                }
            }
        });
        this.table.add(this.btnJugar).size((WORLD_WIDTH / ESCALA_ANCHO_BOTON),(WORLD_HEIGHT /ESCALA_ALTO_BOTON)).expandX().left().padBottom(PAD_BOTTOM);

        this.btnCreditos = crearBoton("botoncreditos", "botoncreditospulsado", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                musica.stop();
                salir(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new CreditsScreen(getParent(),skin));
                    }
                });
            }
        });
        this.table.add(this.btnCreditos).width(WORLD_WIDTH/ESCALA_ANCHO_BOTON).height(WORLD_HEIGHT/ESCALA_ALTO_BOTON).right();
        this.table.row();

        this.btnOpciones = crearBoton("botonajustes","botonajustespulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                salir(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new SettingsScreen(getParent(),skin,musica));
                    }
                });
            }
        });
        this.table.add(this.btnOpciones).size((WORLD_WIDTH / ESCALA_ANCHO_BOTON),(WORLD_HEIGHT /ESCALA_ALTO_BOTON)).expandX().left().padBottom(PAD_BOTTOM);
        this.table.row();

        this.btnCamara = crearBoton("botoncamara","botoncamarapulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                Main.platformSpecific.hacerFoto();
            }
        });

        this.table.add(this.btnCamara).size((WORLD_WIDTH / ESCALA_ANCHO_BOTON),(WORLD_HEIGHT /ESCALA_ALTO_BOTON)).expandX().left().padBottom(PAD_BOTTOM);

        this.btnPuntuaciones = crearBoton("botonpuntuaciones", "botonpuntuacionespulsado", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                salir(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new ScoresScreen(getParent(),skin));
                    }
                });
            }
        });

        this.table.add(this.btnPuntuaciones).width(WORLD_WIDTH/ESCALA_ANCHO_BOTON).height(WORLD_HEIGHT/ESCALA_ALTO_BOTON).right();

        this.table.row();

        this.btnSalir = crearBoton("botonpuerta","botonpuertapulsado",new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                dialogoSalir.show(getStage());
            }
        });
        this.table.add(this.btnSalir).size((WORLD_WIDTH / ESCALA_ANCHO_BOTON),(WORLD_HEIGHT /ESCALA_ALTO_BOTON)).expandX().left();

        this.btnLeyenda = crearBoton("botonleyenda", "botonleyendapulsado", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.vibrar(100);
                salir(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new Leyenda(getParent(),skin));
                    }
                });
            }
        });

        this.table.add(this.btnLeyenda).width(WORLD_WIDTH/ESCALA_ANCHO_BOTON).height(WORLD_HEIGHT/ESCALA_ALTO_BOTON).right();

        getStage().addActor(this.table);
        Gdx.input.setInputProcessor(getStage());
        this.musica = ((Main)getParent()).manager.get("bensound/menuprincipal.mp3");
        musica.setLooping(true);
        musica.setVolume(0.01f);
        ((Main) getParent()).reproducirMusica(this.musica);

        this.btnJugar.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
        this.btnLeyenda.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
        this.btnPuntuaciones.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
        this.btnCreditos.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
        this.btnCamara.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
        this.btnSalir.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
        this.btnOpciones.addAction(Actions.sequence(Actions.alpha(0f),Actions.fadeIn(1.25f)));
    }

    /**
     * Crea un botón con un estilo que utiliza las imagenes dadas y un ChangeListener como acción a realizar.
     * @param up Nombre de la imagen del botón sin pulsar en el atlas de texturas.
     * @param down Nombre de la imagen del botón pulsado en el atlas de texturas
     * @param listener Listener que proporciona al botón una acción a realizar cuando se pulsa.
     * @return Botón creado.
     */
    public Button crearBoton(String up,String down,ChangeListener listener) {
        Button.ButtonStyle style = new Button.ButtonStyle();
        style.up = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(up)));
        style.down = new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion(down)));
        Button test = new Button(style);
        test.addListener(listener);
        return test;
    }

    /**
     * Lee la configuración del archivo de configuración si este existe.
     */
    private void leerConfiguracion() {
        InputStream in = null;
        DataInputStream input = null;
        try{
            in = Gdx.files.local("config").read();
            input =  new DataInputStream(in);
            boolean i;
            while(true){
                i = input.readBoolean();
                Main.desactivarMusica = i;
                i = input.readBoolean();
                Main.fps = i;
                i = input.readBoolean();
                Main.desactivarVibracion = i;
                this.imagenFondo = new Texture(Gdx.files.local(input.readUTF()));
            }
        }catch (EOFException e){
            if(input != null){
                try {
                    input.close();
                } catch (IOException ioException) {
                    System.err.println(ioException.getLocalizedMessage());
                }
            }
        }
        catch(GdxRuntimeException | IOException e ){
            System.err.println(e.getLocalizedMessage());
        }finally {
            if(in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    System.err.println(e.getLocalizedMessage());
                }
            }
        }
    }

    /**
     * Inicia la secuencia de salida de la pantalla.
     */
    private void salir(Runnable run){
        getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(run)));
    }

    /**
     * Renderiza la escena de está pantalla.
     * @param delta Tiempo que ha transcurrido desde el último frame.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0.0f,0.0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        getStage().act(delta);
        getStage().draw();
    }


    /**
     * Gestiona la reapertura de la pantalla.
     */
    @Override
    public void resume() {
        super.resume();
        ((Main) getParent()).reproducirMusica(this.musica);
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        getStage().dispose();
        this.musica.dispose();
    }
}
