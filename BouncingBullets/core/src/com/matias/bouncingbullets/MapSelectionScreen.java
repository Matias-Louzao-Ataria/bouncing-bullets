package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Pantalla para la selección de nivel
 */
public class MapSelectionScreen extends BaseScreen{

    /**
     * Alto de la pantalla.
     */
    private final float WORLD_HEIGHT = Gdx.graphics.getHeight();

    /**
     * Ancho de la pantalla.
     */
    private final float WORLD_WIDTH = WORLD_HEIGHT*Main.ASPECT_RATIO;

    /**
     * Mapas generados.
     */
    private Array<Mapa> mapas;

    /**
     * Tabla de la pantalla.
     */
    private Table tabla;

    /**
     * Número de botones generados.
     */
    private int numMapas = 0;

    /**
     * Scroll de la pantalla.
     */
    private ScrollPane scrollPane;

    /**
     * Aspecto de los elementos de la interfaz de usuario de la pantalla.
     */
    private Skin skin;

    /**
     * Imagen que se desea poner de fondo en el juego.
     */
    private Texture imagenFondo = null;

    /**
     * Música de la pantalla.
     */
    private Music musica;

    /**
     * Constructor del modelo base de las pantallas del juego.
     *
     * @param parent Juego al que pertenece.
     */
    public MapSelectionScreen(Game parent, Skin skin) {
        super(parent);
        this.skin = skin;
    }

    /**
     * Constructor de MapSelectionScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de los elementos de interfaz de usuario.
     * @param imagenFondo Imagen que se desea poner de fondo en el juego.
     * @param musica Música de la pantalla.
     */
    public MapSelectionScreen(Game parent, Skin skin, Texture imagenFondo,Music musica) {
        super(parent);
        this.imagenFondo = imagenFondo;
        this.skin = skin;
        this.musica = musica;
    }

    /**
     * Constructor de MapSelectionScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de los elementos de interfaz de usuario.
     * @param musica Música de la pantalla.
     */
    public MapSelectionScreen(Game parent, Skin skin, Music musica) {
        super(parent);
        this.skin = skin;
        this.musica = musica;
    }

    /**
     * Inicializa los elementos de la interfaz de usuario y añade sus funcionalidades.
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT)));
        this.mapas = new Array<Mapa>();

        this.tabla = new Table();
        this.tabla.setFillParent(true);
        this.scrollPane = new ScrollPane(this.tabla,this.skin);
        this.scrollPane.setFillParent(false);
        this.scrollPane.setSize(WORLD_WIDTH,WORLD_HEIGHT);
        this.scrollPane.setScrollingDisabled(true,false);

        try{
            FileHandle[] jsonFiles = null;

            switch (Gdx.app.getType()){
                case Android:
                    jsonFiles = Gdx.files.internal("").list("json");
                    break;
                case Desktop:
                    jsonFiles = Gdx.files.local("").list("json");
                    break;
            }

            for(int i = 0;i < jsonFiles.length;i++){
                FileHandle jsonFileActual = Gdx.files.internal("mapa"+i+".json");
                if(this.numMapas % 2 == 0 && numMapas != 0){
                    this.tabla.row();
                }

                cargarMapa(jsonFileActual);

                final Mapa mapaActual = mapas.get(i);
                TextButton b = new TextButton(Main.idioma.get("nombremapa")+" "+mapaActual.nombre+"\n"+Main.idioma.get("dificultad")+" "+ mapaActual.dificultad+"\n"+Main.idioma.get("balas")+" "+ mapaActual.maxBalas,this.skin);
                b.addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        if(musica != null){
                            musica.stop();
                        }

                        Screen pantalla = null;

                        if(!Main.isFirts){
                            if(imagenFondo != null){
                                pantalla = new MainGameScreen(getParent(),mapaActual,imagenFondo);
                            }else{
                                pantalla = new MainGameScreen(getParent(),mapaActual);
                            }
                        }else{
                            if(imagenFondo != null){
                                pantalla = new PresentationScreen(getParent(),skin,mapaActual,imagenFondo);
                            }else{
                                pantalla = new PresentationScreen(getParent(),skin,mapaActual);
                            }
                        }
                        final Screen temp = pantalla;
                        getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                getParent().setScreen(temp);
                            }
                        })));
                    }
                });
                this.tabla.add(b).width(WORLD_WIDTH/2);
                this.numMapas++;
            }

            getStage().addActor(this.scrollPane);
            getStage().addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1f)));
            Gdx.input.setInputProcessor(getStage());
        }catch(GdxRuntimeException e){
            System.err.println(e.getLocalizedMessage());
        }
    }

    /**
     * Muestra la pantalla.
     * @param delta Tiempo que ha pasado desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act(delta);
        getStage().draw();
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Carga el mapa que se desea jugar.
     */
    private void cargarMapa(FileHandle jsonFile) {
        Json json = new Json();
        this.mapas.add(json.fromJson(Mapa.class,jsonFile));
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        getStage().dispose();
    }
}
