package com.matias.bouncingbullets;

/**
 * Interfaz con metodos para la gestión de funciones específicas de cada plataforma.
 */
public interface PlatformSpecific {

    /**
     * Metodo para acceder a la camara de la plataforma en la que se ejecuta el juego.
     */
    public void hacerFoto();

    /**
     * Metodo para mostrar mensajes de error al usuario de la plataforma en la que se ejecuta el juego.
     * @param msg Mensaje a mostrar.
     */
    public void avisarUsuario(String msg);

    /**
     * Metodo para vibrar de la plataforma en la que se ejecuta el juego.
     * @param miliseconds milisegundos que debe vibrar.
     */
    public void vibrar(int miliseconds);


    /**
     * Metodo para vibrar con un patrón de la plataforma en la que se ejecuta el juego.
     * @param patern Patrón que se desea seguir al vibrar.
     * @param repeat Posición desde la que se quiere repetir la vibración, -1 si no se quiere repetir.
     */
    public void vibrar(long[] patern,int repeat);
}
