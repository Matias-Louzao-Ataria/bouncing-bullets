package com.matias.bouncingbullets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Actor powerUp que aparece en el mapa.
 */
public class PowerUpObject extends BaseActor{

    /**
     * Identificador para los powerUps en la pantalla.
     */
    public static enum TipoObj{
        corazon,
        boton,
        chaleco
    };

    /**
     * Tiempo que tarda el powerUp en desaparecer.
     */
    private int timer = 10;

    /**
     * Momento en el que apareció en la pantalla.
     */
    private long segSpawn = 0;

    /**
     * Indica si el powerUp debe desaparecer en el siguiente fotograma.
     */
    private boolean desaparecer = false;

    /**
     * Tipo de powerUp.
     */
    private PowerUpObject.TipoObj tipo;

    /**
     *
     * @param world Mundo al que pertenece el powerUp.
     * @param texture Textura del powerUp.
     * @param objType Tipo de powerUp.
     * @param nanoseconds Momento en el que apareció en la pantalla.
     */
    public PowerUpObject(World world, TextureRegion texture, TipoObj objType, long nanoseconds) {
        super(world,texture);

        this.segSpawn = nanoseconds;
        this.tipo = objType;

        setWIDTH(1f);
        setHEIGHT(1f);


    }

    /**
     * Ejecuta la actuación del actor
     * @param delta Tiempo que ha parasado desde el último fotograma.
     */
    @Override
    public void act(float delta) {
        if(TimeUtils.nanoTime() - segSpawn >= timer * 1000000000L){
            desaparecer = true;
        }
    }

    /**
     * Dibuja el actor y lo hace parpadear cuando está a punto de desaparecer.
     * @param batch Batch para dibujar el actor.
     * @param parentAlpha Transparencia del actor.
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        int a = timer-3;
        if(TimeUtils.nanoTime() - segSpawn < a * 1000000000L){
            dibujuar(batch);
        }else{
            if(TimeUtils.nanoTime() % 2 == 0){
                dibujuar(batch);
            }
        }
    }

    /**
     * Dibuja el actor
     * @param batch Batch para dibujar el actor.
     */
    private void dibujuar(Batch batch) {
        this.setPosition(this.body.getPosition().x- getWIDTH(),this.body.getPosition().y- getHEIGHT());
        batch.draw(this.texture,getX(),getY(),getWidth(),getHeight());
    }

    /**
     * Asigna una posición al powerUP en el mapa.
     * @param posicion Posición a asignar.
     */
    public void setPosicion(Vector2 posicion){
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(posicion);
        bodyDef.type = BodyDef.BodyType.StaticBody;
        this.body = this.world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(getWIDTH(), getWIDTH());
        fixtureDef.shape = shape;
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = CategoryBits.OBJETO;
        fixtureDef.filter.maskBits = CategoryBits.JUGADOR;
        fixtureDef.filter.groupIndex = 0;
        this.fixture = this.body.createFixture(fixtureDef);
        this.fixture.setUserData(this);
        shape.dispose();
        this.setSize(getWIDTH()*2,getHEIGHT()*2);
    }

    /**
     * Devuelve la altura del actor.
     * @return Altura del actor.
     */
    @Override
    public float getHEIGHT() {
        return super.getHEIGHT();
    }

    /**
     * Devuelve la anchura del actor.
     * @return Anchura del actor.
     */
    @Override
    public float getWIDTH() {
        return super.getWIDTH();
    }

    /**
     * Devuelve el timer del actor.
     * @return Timer del actor.
     */
    public int getTimer() {
        return timer;
    }

    /**
     * Establece la duración de un objeto.
     * @param timer Duración del objeto.
     */
    public void setTimer(int timer){
        this.timer = timer;
    }

    /**
     * Devuelve cuando se generó el actor.
     * @return Cuando se generó el actor.
     */
    public long getSegSpawn() {
        return segSpawn;
    }

    /**
     * Establece cuando se creó el powerUp o cuando se quitó la pausa.
     * @param segSpawn Momento en el que pasa.
     */
    public void setSegSpawn(long segSpawn) {
        this.segSpawn = segSpawn;
    }

    /**
     * Devuelve si el actor tiene que desaparecer.
     * @return Si el actor tiene que desaparecer.
     */
    public boolean isDesaparecer() {
        return desaparecer;
    }

    /**
     * Devuelve el tipo de powerUp.
     * @return Tipo de powerUp.
     */
    public TipoObj getTipo() {
        return tipo;
    }

    /**
     * Establece si el powerUp tiene que desaparecer en el siguiente fotograma.
     */
    public void setDesaparecer(boolean desaparecer) {
        this.desaparecer = desaparecer;
    }

    /**
     * Devuelve el cuerpo del actor.
     * @return El cuerpo del actor.
     */
    public Body getBody() {
        return body;
    }

    /**
     * Libera los recursos del actor.
     */
    @Override
    public void dispose() {
        super.dispose();
    }

    /**
     * Compara powerUps dependiendo de su body y su fixture.
     * @param o PowerUp a comprarar.
     * @return Si son iguales o no.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()){
            return false;
        }
        PowerUpObject powerUp = (PowerUpObject) o;
        return this.body == powerUp.body && this.fixture == powerUp.fixture;
    }
}
