package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Pantalla de las puntuaciones
 */
public class ScoresScreen extends BaseScreen{

    /**
     * Altura de la pantalla.
     */
    private final float WORLD_HEIGHT = Gdx.graphics.getHeight();

    /**
     * Anchura de la pantalla
     */
    private final float WORLD_WIDTH = WORLD_HEIGHT*Main.ASPECT_RATIO;

    /**
     * Tabla de la pantalla
     */
    private Table tabla;

    /**
     * Scroll de la pantalla.
     */
    private ScrollPane scrollPane;

    /**
     * Puntuaciones leídas del archivo de puntuaciones.
     */
    private String puntuacionesStr = "";

    /**
     * Mapas leídos de los archivos de mapas.
     */
    private Array<Mapa> mapas;

    /**
     * Aspecto de la intefaz de usuario de la pantalla.
     */
    private Skin skin;

    /**
     * Botón de salir de la pantalla.
     */
    TextButton btnSalir;

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     */
    public ScoresScreen(Game parent, Skin skin) {
        super(parent);
        this.skin = skin;
    }

    /**
     * Inicializa los componentes de la interfaz de usuario de la pantalla.
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT)));
        FileHandle inputPuntuaciones = Gdx.files.local("puntuaciones.txt");

        this.btnSalir = new TextButton(Main.idioma.get("salida"),this.skin);
        this.btnSalir.setSize(WORLD_WIDTH,(WORLD_HEIGHT/2)*0.25f);
        this.btnSalir.setPosition(0,WORLD_HEIGHT);
        this.btnSalir.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new MainMenuScreen(getParent()));
                    }
                })));
            }
        });
        getStage().addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1f)));
        this.btnSalir.addAction(Actions.moveBy(0,-this.btnSalir.getHeight(),1f));

        if(inputPuntuaciones.exists() && inputPuntuaciones.length() > 0) {
            this.tabla = new Table();
            this.tabla.setFillParent(false);
            this.tabla.setSize(WORLD_WIDTH,(WORLD_HEIGHT/2)*1.75f);
            this.tabla.padLeft(30);
            this.scrollPane = new ScrollPane(this.tabla, this.skin);
            this.scrollPane.setFillParent(false);
            this.scrollPane.setSize(this.tabla.getWidth(),this.tabla.getHeight());
            this.scrollPane.setScrollingDisabled(true, false);

            this.tabla.pack();

            leerPuntuaciones(inputPuntuaciones);

            Label.LabelStyle style = new Label.LabelStyle();
            style.font = Main.bitmapFont;
            style.font.getData().setScale(2f,2f);
            Label l1 = new Label(Main.idioma.get("mapas"), style);
            l1.setWrap(true);

            this.tabla.add(l1).width(WORLD_WIDTH * 2 / 3).center();
            l1 = new Label(Main.idioma.get("puntuaciones"), style);
            l1.setWrap(true);
            this.tabla.add(l1).width(WORLD_WIDTH / 3).center();
            this.tabla.row();

            String[] puntuaciones = this.puntuacionesStr.split(",");
            for (String str : puntuaciones) {
                if (str.contains(";")) {
                    String[] puntuacion = str.split(";");
                    Label l = new Label(puntuacion[0], style);
                    Label l2 = new Label(puntuacion[1], style);
                    this.tabla.add(l).width(WORLD_WIDTH * 2 / 3);
                    this.tabla.add(l2).width(WORLD_WIDTH / 3);
                } else {
                    Label l = new Label("", style);
                    Label l2 = new Label(str, style);
                    this.tabla.add(l).width(WORLD_WIDTH * 2 / 3);
                    this.tabla.add(l2).width(WORLD_WIDTH / 3);
                }
                this.tabla.row();
                this.tabla.addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1.25f)));
            }
            getStage().addActor(this.btnSalir);
            getStage().addActor(this.scrollPane);
        }else {
            Label a = new Label(Main.idioma.get("nopuntuaciones"),this.skin);
            a.setWrap(true);
            getStage().addActor(this.btnSalir);
            getStage().addActor(a);
        }

        Gdx.input.setInputProcessor(getStage());
    }

    /**
     * Lee el archivo de puntuaciones.
     */
    private void leerPuntuaciones(FileHandle inputPuntuacion) {
        try {
            InputStream in = inputPuntuacion.read();
            BufferedInputStream input = new BufferedInputStream(in);
            int leido = 0;
            while ((leido = input.read()) != -1) {
                puntuacionesStr += (char) leido;
            }
        } catch (GdxRuntimeException | IOException e) {
            Main.platformSpecific.avisarUsuario(Main.idioma.get("errorarchivoleer"));
        }
    }

    /**
     * Renderiza la pantalla
     * @param delta Tiempo que ha pasado desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act(delta);
        getStage().draw();
    }

    /**
     * Ajusta la pantalla a un nuevo tamaño
     * @param width Nuevo ancho
     * @param height Nuevo alto
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        getStage().dispose();
    }
}
