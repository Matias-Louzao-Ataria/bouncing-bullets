package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.io.File;

/**
 * Pantalla de selección de fondo para el mapa.
 */
public class SelectionScreen extends BaseScreen{

    /**
     * Tabla que contendrá los elementos de la interfaz de usuario de la pantalla.
     */
    private Table table;

    /**
     * Scroll que contendrá la tabla.
     */
    private ScrollPane scrollPane;

    /**
     * Aspecto para los elementos de la interfaz de usurio.
     */
    private final Skin skin;

    /**
     * Número de imágenes.
     */
    private int numImg = 0;

    /**
     * Imagen seleccionada.
     */
    private String selectedImage = null;

    /**
     * Música de la pantalla.
     */
    private Music musica;

    /**
     * Imagenes tomadas por la camara.
     */
    private Array<SpriteDrawable> imagenes;

    /**
     * Escena a renderizar para mostrar el progreso de la carga de imágenes.
     */
    private Stage loadingStage;

    /**
     * Constructor de SelectionScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de los elementos de interfaz de usuario.
     */
    public SelectionScreen(Game parent, Skin skin) {
        super(parent);
        this.skin = skin;
    }

    /**
     * Constructor de SelectionScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de los elementos de interfaz de usuario.
     * @param musica Música de la pantalla.
     */
    public SelectionScreen(Game parent, Skin skin,Music musica) {
        super(parent);
        this.skin = skin;
        this.musica = musica;
    }

    /**
     * Genera los elemento de la interfaz de usuario de la pantalla.
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(Gdx.graphics.getHeight()*16/9f,Gdx.graphics.getHeight())));

        this.imagenes = new Array<SpriteDrawable>();

        this.table = new Table();
        this.table.setFillParent(true);

        this.scrollPane = new ScrollPane(this.table,this.skin);
        this.scrollPane.setFillParent(true);

        cargarImagenes();

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();

        TextButton btnAceptar = new TextButton(Main.idioma.get("aceptar"),this.skin);
        btnAceptar.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(selectedImage != null){
                    getParent().setScreen(new SettingsScreen(getParent(),skin,musica,selectedImage));
                }
            }
        });
        btnAceptar.setPosition(0,Gdx.graphics.getHeight()-btnAceptar.getHeight());

        TextButton btnRechazar = new TextButton(Main.idioma.get("rechazar"),this.skin);
        btnRechazar.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getParent().setScreen(new SettingsScreen(getParent(),skin,musica));
            }
        });
        btnRechazar.setPosition(Gdx.graphics.getHeight()*16/9f-btnRechazar.getWidth(),Gdx.graphics.getHeight()-btnRechazar.getHeight());

        TextButton btnFoto = new TextButton(Main.idioma.get("agregar"),this.skin);
        btnFoto.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.platformSpecific.hacerFoto();
            }
        });
        btnFoto.setPosition((Gdx.graphics.getHeight()*16/9f - btnFoto.getWidth())/2f,Gdx.graphics.getHeight() - btnFoto.getHeight());

        this.scrollPane.setScrollingDisabled(true,false);
        getStage().addActor(this.scrollPane);

        generarBotones();
        getStage().addActor(btnFoto);
        getStage().addActor(btnAceptar);
        getStage().addActor(btnRechazar);
        Gdx.input.setInputProcessor(getStage());
        this.table.addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1.25f)));
    }

    /**
     * Carga las imagenes.
     */
    private void cargarImagenes() {
        FileHandle carpetaLocal = new FileHandle(new File(Gdx.files.getLocalStoragePath()));
        FileHandle[] archivos = carpetaLocal.list("jpeg");
        if(this.imagenes.size > 0){
            if(this.imagenes.size < archivos.length){
                for (int i = this.imagenes.size; i < archivos.length; i++) {
                    this.imagenes.add(new SpriteDrawable(new Sprite(new Texture(archivos[i]))));
                }
            }
        }else{
            for (final FileHandle file : archivos) {
                this.imagenes.add(new SpriteDrawable(new Sprite(new Texture(file))));
            }
        }
    }

    /**
     * Gestiona la reapertura de la pantalla.
     */
    @Override
    public void resume() {
        super.resume();
    }

    /**
     * Genera los botones que representan las imágenes tomadas por la cámara.
     */
    private void generarBotones() {

        FileHandle carpetaLocal = new FileHandle(new File(Gdx.files.getLocalStoragePath()));
        final FileHandle[] archivos = carpetaLocal.list("jpeg");
        int i = 0,limit = 0;
        if(this.table.getChildren().size < archivos.length){
            i = this.table.getChildren().size;
            limit = archivos.length;
        }else if(!this.table.hasChildren()){
            limit = archivos.length;

        }

        for (;i < limit;i++) {
            final SpriteDrawable actual = this.imagenes.get(i);
            if(this.numImg % 4 == 0){
                this.table.row();
            }
            final ImageButton btnImagen = new ImageButton(actual);
            final int tempi = i;
            btnImagen.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    selectedImage = archivos[tempi].name();
                    scrollPane.getStyle().background = actual;
                }
            });

            this.table.add(btnImagen).width((Gdx.graphics.getHeight()*16/9f)/4).height(Gdx.graphics.getHeight()/4f).padBottom(5f).padLeft(5f);
            this.numImg++;
        }
    }

    /**
     * Renderiza la escena.
     * @param delta Tiempo que ha pasado desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        getStage().act(delta);
        getStage().draw();
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
        cargarImagenes();
        generarBotones();
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        for (SpriteDrawable s: this.imagenes) {
            s.getSprite().getTexture().dispose();
        }
        getStage().dispose();
    }
}
