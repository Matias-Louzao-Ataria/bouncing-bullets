package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Pantalla de ajustes.
 */
public class SettingsScreen extends BaseScreen{

    /**
     * Aspecto para los elementos de la interfaz de usurio.
     */
    private Skin skin;

    /**
     * Fondo del menú
     */
    private TextureAtlas.AtlasRegion mapa = ((Main)getParent()).textureAtlas.findRegion("cuadrado");

    /**
     * Botones del menú.
     */
    private TextButton btnmusica, btnvibracion, btnfps,btnMapa;

    /**
     * Botón del menú que aplica las opciones elegidas.
     */
    private TextButton btnAtras;

    /**
     * Tabla de la pantalla.
     */
    private Table table;

    /***
     * Música de la pantalla.
     */
    private Music musica;

    /**
     * Fondo seleccionado en la pantalla de cambio de mapa.
     */
    String fondoSeleccionado = null;

    /**
     * Constructor de SettingsScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de los elementos de interfaz de usuario.
     * @param musica Música de la pantalla.
     */
    public SettingsScreen(Game parent, Skin skin, Music musica) {
        super(parent);
        this.skin = skin;
        this.musica = musica;
    }

    /**
     * Constructor de SettingsScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de los elementos de interfaz de usuario.
     * @param musica Música de la pantalla.
     * @param fondo Fondo a utilizar en la pantalla principal del juego.
     */
    public SettingsScreen(Game parent, Skin skin, Music musica,String fondo){
        this(parent, skin, musica);
        this.fondoSeleccionado = fondo;
    }

    /**
     *  Inicializa el stage que se va a utilizar en esta pantalla, una tabla y los componentes de interfaz de usuario que la compondrán.
     *  Además se encarga de cargar la configuración guardada en un archivo binario si es que existe dicho archivo.
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(Gdx.graphics.getHeight()*Main.ASPECT_RATIO,Gdx.graphics.getHeight())));

        this.table = new Table();
        this.table.background(new SpriteDrawable(new Sprite(((Main)getParent()).textureAtlas.findRegion("cuadrado"))));
        this.table.setFillParent(true);

        this.skin.getFont("default-font").getData().setScale(5f,5f);

        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = Main.bitmapFont;
        style.font.getData().setScale(2f,2f);
        style.up = this.skin.getDrawable("default-round");
        style.down = this.skin.getDrawable("default-round-down");

        this.btnmusica = new TextButton("",style);

        musicaText();
        this.btnmusica.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.desactivarMusica = !Main.desactivarMusica;
                if(Main.desactivarMusica){
                    musica.stop();
                }else{
                    musica.play();
                }
                musicaText();
            }
        });

        this.btnfps = new TextButton("",style);
        fpsText();
        this.btnfps.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.fps = !Main.fps;
                fpsText();
            }
        });

        this.btnvibracion = new TextButton("",style);
        vibracionText();
        this.btnvibracion.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Main.desactivarVibracion = !Main.desactivarVibracion;
                vibracionText();
            }
        });

        this.btnMapa = new TextButton(Main.idioma.get("fondo"),style);
        this.btnMapa.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new SelectionScreen(getParent(),skin,musica));
                    }
                })));
            }
        });

        this.btnAtras = new TextButton(Main.idioma.get("aceptar"),style);
        this.btnAtras.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                try{
                    DataOutputStream output = new DataOutputStream(Gdx.files.local("config").write(false));
                    output.writeBoolean(Main.desactivarMusica);
                    output.writeBoolean(Main.fps);
                    output.writeBoolean(Main.desactivarVibracion);
                    if(fondoSeleccionado != null){
                        output.writeUTF(fondoSeleccionado);
                    }
                    output.close();
                }catch(GdxRuntimeException | IOException e){
                    System.err.println(e.getLocalizedMessage());
                    Main.platformSpecific.avisarUsuario(Main.idioma.get("errorarchivo"));
                }
                getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new MainMenuScreen(getParent()));
                    }
                })));
            }
        });

        this.table.add(this.btnAtras).fillX().expandX().height((Gdx.graphics.getHeight()/2f)*0.25f);
        this.table.row();
        this.table.add(this.btnmusica).height((Gdx.graphics.getHeight()/2f)*1.75f/4f).width(Gdx.graphics.getWidth()/2f);
        this.table.row();
        this.table.add(this.btnfps).height((Gdx.graphics.getHeight()/2f)*1.75f/4f).width(Gdx.graphics.getWidth()/2f);
        this.table.row();
        this.table.add(this.btnvibracion).height((Gdx.graphics.getHeight()/2f)*1.75f/4f).width(Gdx.graphics.getWidth()/2f);
        this.table.row();
        this.table.add(this.btnMapa).height((Gdx.graphics.getHeight()/2f)*1.75f/4f).width(Gdx.graphics.getWidth()/2f);
        getStage().addActor(this.table);
        Gdx.input.setInputProcessor(getStage());
        this.table.addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1.25f)));
    }

    /**
     * Actualiza el texto del botón vibración.
     */
    public void vibracionText() {
        if(Main.desactivarVibracion){
            this.btnvibracion.setText(Main.idioma.get("vibracion"));
        }else{
            this.btnvibracion.setText(Main.idioma.get("vibracion2"));
        }
    }

    /**
     * Actualiza el texto del botón FPS.
     */
    public void fpsText() {
        if(Main.fps){
            this.btnfps.setText(Main.idioma.get("fps2"));
        }else{
            this.btnfps.setText(Main.idioma.get("fps"));
        }
    }

    /**
     * Actualiza el texto del botón Música.
     */
    private void musicaText() {
        if(Main.desactivarMusica){
            this.btnmusica.setText(Main.idioma.get("musica2"));
        }else{
            this.btnmusica.setText(Main.idioma.get("musica"));
        }
    }

    /**
     * Renderiza la escena.
     * @param delta Tiempo que ha pasado desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        getStage().act(delta);
        getStage().draw();
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        getStage().dispose();
    }
}
