package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Pantalla que aparece antes de inicial el juego.
 */
public class SplashScreen extends BaseScreen{

    /**
     * Tabla de la pantalla.
     */
    private Table table;

    /**
     * Etiqueta de la pantalla.
     */
    private Label lbl;

    /**
     * Icono de la pantalla.
     */
    private Image icono;

    /**
     * Constructor de SplashScreen.
     * @param parent Juego al que pertenece.
     */
    public SplashScreen(Game parent) {
        super(parent);
    }

    /**
     * Inicializa los componentes necesarios para la pantalla.
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(Gdx.graphics.getHeight()*Main.ASPECT_RATIO,Gdx.graphics.getHeight())));
        this.table = new Table();
        this.table.setFillParent(true);
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Main.bitmapFont;
        this.lbl = new Label("Bouncing Bullets",style);
        this.lbl.setColor(Color.BLACK);
        icono = new Image(((Main)getParent()).textureAtlas.findRegion("icono"));
        this.table.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.fadeIn(1f),
                Actions.delay(1.5f),
                Actions.fadeOut(1f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getParent().setScreen(new MainMenuScreen(getParent()));
                    }
                })));
        this.lbl.setFontScale(3f,3f);
        this.table.add(this.icono).width((Gdx.graphics.getHeight()*Main.ASPECT_RATIO)/3f).height(Gdx.graphics.getHeight()/3f);
        this.table.row();
        this.table.add(this.lbl);
        getStage().addActor(this.table);
        Gdx.input.setInputProcessor(getStage());
    }

    /**
     * Renderiza la escena de está pantalla.
     * @param delta Tiempo que ha transcurrido desde el último frame.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1,1,1,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act();
        getStage().draw();
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        getStage().dispose();
    }
}
